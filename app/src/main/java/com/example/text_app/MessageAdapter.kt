package com.example.text_app

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.google.firebase.auth.FirebaseAuth
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class MessageAdapter(private val messages: List<ChatMessage>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val MESSAGE_TYPE_SENT = 1
    private val MESSAGE_TYPE_RECEIVED = 2
    private val auth = FirebaseAuth.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == MESSAGE_TYPE_SENT) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_sent, parent, false)
            SentMessageViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_received, parent, false)
            ReceivedMessageViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = messages[position]
        if (holder is SentMessageViewHolder) {
            holder.bind(message)
        } else if (holder is ReceivedMessageViewHolder) {
            holder.bind(message)
        }
    }

    override fun getItemCount() = messages.size

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].senderId == auth.currentUser?.uid) {
            MESSAGE_TYPE_SENT
        } else {
            MESSAGE_TYPE_RECEIVED
        }
    }

    class SentMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val messageText: TextView = itemView.findViewById(R.id.textViewContent)
        private val timestamp: TextView = itemView.findViewById(R.id.textViewTimestamp)
        private val imageMessage: ImageView = itemView.findViewById(R.id.imageViewContent)

        fun bind(message: ChatMessage) {
            messageText.text = message.content
            timestamp.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date(message.timestamp))

            if (message.imageUrl != null) {
                imageMessage.visibility = View.VISIBLE
                messageText.visibility = View.GONE
                imageMessage.load(message.imageUrl)
                imageMessage.setOnClickListener {
                    showFullScreenImage(it.context, message.imageUrl)
                }
            } else {
                imageMessage.visibility = View.GONE
                messageText.visibility = View.VISIBLE
            }
        }
    }

    class ReceivedMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val messageText: TextView = itemView.findViewById(R.id.textViewContent)
        private val timestamp: TextView = itemView.findViewById(R.id.textViewTimestamp)
        private val senderUsername: TextView = itemView.findViewById(R.id.textViewSender)
        private val imageMessage: ImageView = itemView.findViewById(R.id.imageViewContent)

        fun bind(message: ChatMessage) {
            messageText.text = message.content
            timestamp.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date(message.timestamp))
            senderUsername.text = message.senderUsername

            if (message.imageUrl != null) {
                imageMessage.visibility = View.VISIBLE
                messageText.visibility = View.GONE
                imageMessage.load(message.imageUrl)
                imageMessage.setOnClickListener {
                    showFullScreenImage(it.context, message.imageUrl)
                }
            } else {
                imageMessage.visibility = View.GONE
                messageText.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        fun showFullScreenImage(context: Context, imageUrl: String) {
            val intent = Intent(context, FullScreenImageActivity::class.java).apply {
                putExtra("imageUrl", imageUrl)
            }
            context.startActivity(intent)
        }
    }
}
