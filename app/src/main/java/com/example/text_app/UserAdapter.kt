package com.example.text_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load

data class User(
    val uid: String = "",
    val username: String = "",
    val profilePictureUrl: String = ""
)

class UserAdapter(private val users: List<User>, private val clickListener: (User) -> Unit) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(users[position], clickListener)
    }

    override fun getItemCount(): Int = users.size

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val avatarImageView: ImageView = itemView.findViewById(R.id.imageViewAvatar)
        private val usernameTextView: TextView = itemView.findViewById(R.id.textViewUsername)

        fun bind(user: User, clickListener: (User) -> Unit) {
            usernameTextView.text = user.username
            if (user.profilePictureUrl.isNotEmpty()) {
                avatarImageView.load(user.profilePictureUrl)
            } else {
                avatarImageView.setImageResource(R.drawable.ic_user_placeholder)
            }
            itemView.setOnClickListener { clickListener(user) }
        }
    }
}
