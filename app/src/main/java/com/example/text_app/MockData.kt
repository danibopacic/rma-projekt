package com.example.text_app

object MockData {

    fun createMockUsers(): List<User> {
        return listOf(
            User("user1", "testuser1", "https://randomuser.me/api/portraits/men/1.jpg"),
            User("user2", "testuser2", "https://randomuser.me/api/portraits/men/2.jpg"),
            User("user3", "testuser3", "https://randomuser.me/api/portraits/men/3.jpg")
        )
    }

    fun createMockMessages(): List<ChatMessage> {
        return listOf(
            ChatMessage("user1", "testuser1", "user2", "Hello, this is a test message from user1 to user2.", 1627550721000L, null),
            ChatMessage("user2", "testuser2", "user1", "Hello, this is a reply from user2 to user1.", 1627550821000L, null),
            ChatMessage("user1", "testuser1", "user3", "Hello, this is a test message from user1 to user3.", 1627550921000L, null)
        )
    }
}
