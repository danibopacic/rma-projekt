package com.example.text_app

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var recyclerViewUsers: RecyclerView
    private lateinit var recyclerViewUnread: RecyclerView
    private lateinit var userAdapter: UserAdapter
    private lateinit var unreadAdapter: UserAdapter
    private lateinit var users: MutableList<User>
    private lateinit var unreadUsers: MutableList<User>
    private var currentUser: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        auth = FirebaseAuth.getInstance()

        if (auth.currentUser == null) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
            return
        }

        setContentView(R.layout.main_activity)

        db = FirebaseFirestore.getInstance()

        val searchView: SearchView = findViewById(R.id.searchViewUsers)
        recyclerViewUsers = findViewById(R.id.recyclerViewUsers)
        recyclerViewUsers.layoutManager = LinearLayoutManager(this)
        users = mutableListOf()
        userAdapter = UserAdapter(users) { user ->
            val intent = Intent(this, SendMessageActivity::class.java)
            intent.putExtra("userId", user.uid)
            intent.putExtra("username", user.username)
            startActivity(intent)
        }
        recyclerViewUsers.adapter = userAdapter

        recyclerViewUnread = findViewById(R.id.recyclerViewUnread)
        recyclerViewUnread.layoutManager = LinearLayoutManager(this)
        unreadUsers = mutableListOf()
        unreadAdapter = UserAdapter(unreadUsers) { user ->
            markMessagesAsRead(user.uid)
            val intent = Intent(this, SendMessageActivity::class.java)
            intent.putExtra("userId", user.uid)
            intent.putExtra("username", user.username)
            startActivity(intent)
        }
        recyclerViewUnread.adapter = unreadAdapter

        searchView.queryHint = "Search users..."
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { searchUsers(it) }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { searchUsers(it) }
                return false
            }
        })

        loadCurrentUser()
        loadUnreadMessages()
    }

    private fun loadCurrentUser() {
        val uid = auth.currentUser?.uid ?: return
        db.collection("users").document(uid).get()
            .addOnSuccessListener { document ->
                currentUser = document.toObject(User::class.java)
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Failed to load current user: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun searchUsers(query: String) {
        val uid = auth.currentUser?.uid ?: return
        db.collection("users")
            .whereGreaterThanOrEqualTo("username", query)
            .whereLessThanOrEqualTo("username", query + "\uf8ff")
            .get()
            .addOnSuccessListener { snapshot ->
                users.clear()
                for (document in snapshot.documents) {
                    val user = document.toObject(User::class.java)
                    if (user != null && user.uid != currentUser?.uid) {
                        users.add(user)
                    }
                }
                userAdapter.notifyDataSetChanged()
                if (users.isEmpty()) {
                    Toast.makeText(this, "No users found", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Failed to search users: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun loadUnreadMessages() {
        val uid = auth.currentUser?.uid ?: return

        db.collection("messages")
            .whereEqualTo("receiverId", uid)
            .whereEqualTo("read", false)
            .orderBy("timestamp", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { snapshot ->
                val userIds = HashSet<String>()
                for (document in snapshot.documents) {
                    val message = document.toObject(ChatMessage::class.java)
                    message?.senderId?.let { userIds.add(it) }
                }
                if (userIds.isNotEmpty()) {
                    loadUnreadMessageUsers(userIds)
                } else {
                    unreadUsers.clear()
                    unreadAdapter.notifyDataSetChanged()
                }
            }
            .addOnFailureListener { e ->
                Log.e("MainActivity", "Failed to load unread messages: ${e.message}", e)
                Toast.makeText(this, "Failed to load unread messages: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun loadUnreadMessageUsers(userIds: Set<String>) {
        db.collection("users")
            .whereIn("uid", userIds.toList())
            .get()
            .addOnSuccessListener { snapshot ->
                unreadUsers.clear()
                for (document in snapshot.documents) {
                    val user = document.toObject(User::class.java)
                    if (user != null) {
                        unreadUsers.add(user)
                    }
                }
                unreadAdapter.notifyDataSetChanged()
            }
            .addOnFailureListener { e ->
                Log.e("MainActivity", "Failed to load unread message users: ${e.message}", e)
                Toast.makeText(this, "Failed to load unread message users: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun markMessagesAsRead(senderId: String) {
        val uid = auth.currentUser?.uid ?: return
        db.collection("messages")
            .whereEqualTo("senderId", senderId)
            .whereEqualTo("receiverId", uid)
            .whereEqualTo("read", false)
            .get()
            .addOnSuccessListener { snapshot ->
                for (document in snapshot.documents) {
                    db.collection("messages").document(document.id).update("read", true)
                }
                loadUnreadMessages()
            }
            .addOnFailureListener { e ->
                Log.e("MainActivity", "Failed to mark messages as read: ${e.message}", e)
                Toast.makeText(this, "Failed to mark messages as read: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_logout -> {
                auth.signOut()
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
