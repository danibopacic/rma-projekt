package com.example.text_app

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var storage: FirebaseStorage
    private var selectedImageUri: Uri? = null
    private lateinit var imageView: ImageView
    private lateinit var pickImageLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_layout)

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance()

        val usernameField: EditText = findViewById(R.id.editTextText)
        val emailField: EditText = findViewById(R.id.editTextTextEmailAddress)
        val passwordField: EditText = findViewById(R.id.editTextTextPassword)
        val confirmPasswordField: EditText = findViewById(R.id.editTextTextPassword2)
        val registerButton: Button = findViewById(R.id.button)
        val selectPictureButton: Button = findViewById(R.id.buttonSelectPicture)
        imageView = findViewById(R.id.imageView)

        pickImageLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if (result.resultCode == RESULT_OK && result.data != null) {
                selectedImageUri = result.data!!.data
                imageView.setImageURI(selectedImageUri)
            }
        }

        selectPictureButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            pickImageLauncher.launch(intent)
        }

        registerButton.setOnClickListener {
            val username = usernameField.text.toString()
            val email = emailField.text.toString()
            val password = passwordField.text.toString()
            val confirmPassword = confirmPasswordField.text.toString()

            if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty() && password == confirmPassword) {
                if (selectedImageUri != null) {
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                val userId = auth.currentUser!!.uid
                                val profilePicRef = storage.reference.child("profilePictures/$userId.jpg")
                                profilePicRef.putFile(selectedImageUri!!)
                                    .addOnSuccessListener {
                                        profilePicRef.downloadUrl.addOnSuccessListener { downloadUrl ->
                                            val user = hashMapOf(
                                                "uid" to userId,
                                                "username" to username,
                                                "profilePictureUrl" to downloadUrl.toString()
                                            )
                                            db.collection("users").document(userId)
                                                .set(user)
                                                .addOnSuccessListener {
                                                    Toast.makeText(this, "Registracija uspješna", Toast.LENGTH_SHORT).show()
                                                    val intent = Intent(this, MainActivity::class.java)
                                                    startActivity(intent)
                                                    finish()
                                                }
                                                .addOnFailureListener { e ->
                                                    Toast.makeText(this, "Failed to register: ${e.message}", Toast.LENGTH_SHORT).show()
                                                }
                                        }
                                    }
                                    .addOnFailureListener { e ->
                                        Toast.makeText(this, "Failed to upload profile picture: ${e.message}", Toast.LENGTH_SHORT).show()
                                    }
                            } else {
                                Toast.makeText(this, "Registration Failed: ${task.exception?.message}", Toast.LENGTH_SHORT).show()
                            }
                        }
                } else {
                    Toast.makeText(this, "Please select a profile picture", Toast.LENGTH_SHORT).show()
                }
            } else {
                if (password != confirmPassword) {
                    Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Please fill out all fields", Toast.LENGTH_SHORT).show()
                }
            }
        }

        val backButton: Button = findViewById(R.id.button2)
        backButton.setOnClickListener {
            finish()
        }
    }
}
