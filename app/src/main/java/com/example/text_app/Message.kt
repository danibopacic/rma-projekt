package com.example.text_app

data class ChatMessage(
    val senderId: String = "",
    val senderUsername: String = "",
    val receiverId: String = "",
    val content: String = "",
    val timestamp: Long = 0L,
    val imageUrl: String? = null,
    val messageId: String = "",
    val read: Boolean = false
)
