package com.example.text_app

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream

class SendMessageActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var recyclerViewMessages: RecyclerView
    private lateinit var messageAdapter: MessageAdapter
    private lateinit var messages: MutableList<ChatMessage>
    private var currentUser: User? = null

    private val CAMERA_REQUEST_CODE = 1001

    private val takePictureLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val imageBitmap = data?.extras?.get("data") as Bitmap
                uploadImageToFirebase(imageBitmap)
            } else {
                Toast.makeText(this, "Camera capture failed", Toast.LENGTH_SHORT).show()
            }
        }

    private val pickImageLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val imageUri = data?.data
                imageUri?.let {
                    val imageView: ImageView = findViewById(R.id.imageView)
                    imageView.setImageURI(imageUri)
                    uploadImageToFirebase(imageUri)
                }
            } else {
                Toast.makeText(this, "Image selection failed", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_message_activity)

        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()

        recyclerViewMessages = findViewById(R.id.recyclerViewMessages)
        recyclerViewMessages.layoutManager = LinearLayoutManager(this)
        messages = mutableListOf()
        messageAdapter = MessageAdapter(messages)
        recyclerViewMessages.adapter = messageAdapter

        loadCurrentUserAndMessages()

        val editTextMessage: EditText = findViewById(R.id.editTextMessage)
        val cameraIcon: ImageView = findViewById(R.id.cameraIcon)
        val galleryIcon: ImageView = findViewById(R.id.galleryIcon)
        val sendIcon: ImageView = findViewById(R.id.sendIcon)
        val backButton: ImageView = findViewById(R.id.backButton)

        sendIcon.setOnClickListener {
            val content = editTextMessage.text.toString()
            if (content.isNotEmpty()) {
                val message = ChatMessage(
                    senderId = auth.currentUser?.uid ?: "",
                    senderUsername = currentUser?.username ?: "Unknown", // Update this line to set the actual username
                    receiverId = intent.getStringExtra("userId") ?: "",
                    content = content,
                    timestamp = System.currentTimeMillis(),
                    imageUrl = null,
                    messageId = ""
                )
                sendMessage(message)
                editTextMessage.text.clear()
            }
        }

        backButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        cameraIcon.setOnClickListener {
            launchCamera()
        }

        galleryIcon.setOnClickListener {
            pickImageFromGallery()
        }
    }

    private fun loadCurrentUserAndMessages() {
        val uid = auth.currentUser?.uid ?: return
        db.collection("users").document(uid).get()
            .addOnSuccessListener { document ->
                currentUser = document.toObject(User::class.java)
                loadMessages()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Failed to load current user: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun loadMessages() {
        val receiverId = intent.getStringExtra("userId") ?: return
        val senderId = auth.currentUser?.uid ?: return

        db.collection("messages")
            .whereIn("senderId", listOf(senderId, receiverId))
            .whereIn("receiverId", listOf(senderId, receiverId))
            .orderBy("timestamp", Query.Direction.ASCENDING)
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Toast.makeText(this, "Failed to load messages: ${e.message}", Toast.LENGTH_SHORT).show()
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    messages.clear()
                    for (document in snapshot.documents) {
                        val message = document.toObject(ChatMessage::class.java)
                        if (message != null) {
                            messages.add(message)
                        }
                    }
                    messageAdapter.notifyDataSetChanged()
                }
            }
    }

    private fun sendMessage(message: ChatMessage) {
        val messageId = db.collection("messages").document().id
        val messageWithId = message.copy(messageId = messageId)

        db.collection("messages")
            .document(messageId)
            .set(messageWithId)
            .addOnSuccessListener {
                // Message successfully added
            }
            .addOnFailureListener {
                // Handle error
            }
    }

    private fun launchCamera() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
        } else {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takePictureLauncher.launch(takePictureIntent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_REQUEST_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            launchCamera()
        } else {
            Toast.makeText(this, "Camera permission denied", Toast.LENGTH_SHORT).show()
        }
    }

    private fun pickImageFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickImageLauncher.launch(galleryIntent)
    }

    private fun uploadImageToFirebase(bitmap: Bitmap) {
        val storageRef = FirebaseStorage.getInstance().reference
        val imagesRef = storageRef.child("images/${System.currentTimeMillis()}.jpg")

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val uploadTask = imagesRef.putBytes(data)
        uploadTask.addOnSuccessListener { taskSnapshot ->
            taskSnapshot.metadata?.reference?.downloadUrl?.addOnSuccessListener { uri ->
                sendImageMessage(uri.toString())
            }
        }.addOnFailureListener {
            Toast.makeText(this, "Image upload failed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun uploadImageToFirebase(uri: Uri) {
        val storageRef = FirebaseStorage.getInstance().reference
        val imagesRef = storageRef.child("images/${System.currentTimeMillis()}.jpg")

        val uploadTask = imagesRef.putFile(uri)
        uploadTask.addOnSuccessListener { taskSnapshot ->
            taskSnapshot.metadata?.reference?.downloadUrl?.addOnSuccessListener { uri ->
                sendImageMessage(uri.toString())
            }
        }.addOnFailureListener {
            Toast.makeText(this, "Image upload failed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendImageMessage(imageUrl: String) {
        val messageId = db.collection("messages").document().id
        val message = ChatMessage(
            senderId = auth.currentUser?.uid ?: "",
            senderUsername = currentUser?.username ?: "Unknown", // Update this line to set the actual username
            receiverId = intent.getStringExtra("userId") ?: "",
            content = "",
            timestamp = System.currentTimeMillis(),
            imageUrl = imageUrl,
            messageId = messageId
        )
        sendMessage(message)
    }
}